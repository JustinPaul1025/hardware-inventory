<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function () {
    Route::get('/categories-selection', 'CategoryController@getSelection');
    Route::get('/units-selection', 'UnitController@getSelection');

    Route::get('/products', 'ProductController@index')->name('products');
    Route::post('/product-list', 'productController@search');
    Route::get('/product-list', 'ProductController@productList');
    Route::post('/product', 'ProductController@store')->name('product-store');
    Route::put('/product/{product}', 'ProductController@update');
    Route::get('/product/{product}', 'ProductController@show');
    Route::delete('/product/{product}', 'ProductController@delete');
    Route::put('/in-stock/{product}', 'ProductController@inStock');
    Route::get('/product-list/select', 'ProductController@list');

    Route::get('/orders', 'OrderController@index')->name('orders');
    Route::post('/order', 'OrderController@store');
    Route::get('/order-list', 'OrderController@list');
    Route::post('/order-list', 'OrderController@search');
    Route::delete('/order/{order}', 'OrderController@delete');
    Route::get('/order/{order}', 'OrderController@show');
    Route::put('/order/{order}', 'OrderController@update');

    Route::get('/order-items/{order}', 'OrderItemController@orderItems');
    Route::put('/item/{order}', 'OrderItemController@update');
    Route::put('/item-delete/{orderItem}', 'OrderItemController@delete');

    Route::get('/users', 'UserController@index')->name('users');
    Route::post('/user', 'UserController@store');
    Route::get('/users-list', 'UserController@list');
    Route::delete('/user/{user}', 'UserController@delete');
    Route::put('/change-password/{user}', 'UserController@changePassword');
    Route::put('/user/{user}', 'UserController@update');

    Route::view('/utilities', 'utilities.index')->name('utilities');
    Route::post('/category', 'CategoryController@store');
    Route::put('/category/{category}', 'CategoryController@update');
    Route::post('/unit', 'UnitController@store');
    Route::put('/unit/{unit}', 'UnitController@update');

    Route::view('/reports', 'reports.index')->name('reports');
    Route::put('/reports/{sort}', 'ReportsController@sort');
    Route::get('/reports-print/{type}/{sort}', 'ReportsController@print');
    Route::get('/reciept/{order}', 'RecieptController@print');

    Route::view('/app-logs', 'logs.index')->name('logs');
    Route::get('logs', 'LogController@index');
    Route::get('logs-clear', 'LogController@clear');
});

