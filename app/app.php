<?php

namespace App;

class App 
{
    public static function get()
    {
        return [
            'current_user' => auth()->check() ? auth()->user() : null,
        ];
    }
}