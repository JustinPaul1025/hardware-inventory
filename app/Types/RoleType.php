<?php

namespace App\Types;

final class RoleType
{
    const ADMINISTRATOR = 'Administrator';
    const EMPLOYEE = 'Employee';

    public static function toArray(): array
    {
        return [
            self::ADMINISTRATOR,
            self::EMPLOYEE,
        ];
    }
}