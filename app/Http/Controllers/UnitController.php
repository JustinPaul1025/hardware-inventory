<?php

namespace App\Http\Controllers;

use App\Models\Unit;
use Illuminate\Http\Request;
use App\Http\Resources\UnitsCollection;

class UnitController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255', 'unique:units']
        ]);


        $unit = Unit::create([
            'name' => $request->input('name')
        ]);

        activity()
            ->causedBy(auth()->user())
            ->performedOn($unit)
            ->withProperties(['unit' => $unit->name])
            ->log('creating unit');

        return $unit;
    }

    public function update(Unit $unit, Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255', 'unique:categories,name,'.$unit->id]
        ]);

        $from = $unit->name;

        $unit->update([
            'name' => $request->input('name')
        ]);

        activity()
            ->causedBy(auth()->user())
            ->performedOn($unit)
            ->withProperties(['from' => $from, 'to' => $unit->name])
            ->log('updating unit');

        return $unit;
    }

    public function getSelection()
    {
        return new UnitsCollection(Unit::orderBy('name', 'asc')->get());
    }
}
