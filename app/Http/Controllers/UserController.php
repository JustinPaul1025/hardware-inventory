<?php

namespace App\Http\Controllers;

use App\User;
use App\Types\RoleType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function Index()
    {
        return view('users.index');
    }

    public function list()
    {
        return response()->json(User::role('Employee')->get());
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
        ]);

        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
        ]);

        $user->assignRole(RoleType::EMPLOYEE);
        
        return $user;
    }

    public function update(User $user, Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$user->id]
        ]);

        return $user->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
        ]);
    }

    public function delete(User $user)
    {
        return $user->delete();
    }

    public function changePassword(User $user, Request $request)
    {
        $request->validate([
            'password' => ['required', 'string', 'min:6'],
        ]);

        return $user->update([
            'password' => Hash::make($request->input('password'))
        ]); 
    }
}
