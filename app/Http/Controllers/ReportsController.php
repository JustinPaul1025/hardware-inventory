<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\Order\OrderCollection;

class ReportsController extends Controller
{
    public function sort($sort, Request $request)
    {
        if($sort == 'today') {
            $order = Order::whereDate('created_at', Carbon::today())->get();
            return new OrderCollection($order);
        }

        if($sort == 'pick-date') {
            $order = Order::whereDate('created_at', Carbon::parse($request->input('date'))->format('Y-m-d'))->get();
            return new OrderCollection($order);
        }

        if($sort == 'pick-month') {
            $data = Order::select([
                DB::raw('count(id) as `client`'), 
                DB::raw('DATE(created_at) as day'),
                DB::raw('sum(total) as total'),
                DB::raw('sum(discount) as discount')
              ])->groupBy('day')
              // And restrict these results to only those created in the last week
              ->whereMonth('created_at', Carbon::parse($request->input('month'))->format('m'))
              ->get();

            return response()->json($data);
        }
    }

    public function print($type, $sort)
    {
        if($type == 'today') {
            $date = Carbon::today()->format('F d, Y');
            $datas = Order::whereDate('created_at', Carbon::today())->get();
            return view('reports.daily', compact('datas', 'date'));
        }

        if($type == 'pick-date') {
            $date = Carbon::parse($sort)->format('F d, Y');
            $datas = Order::whereDate('created_at', Carbon::parse($sort)->format('Y-m-d'))->get();
            return view('reports.daily', compact('datas', 'date'));
        }

        if($type == 'pick-month') {
            $month = Carbon::parse($sort)->format('F');
            $datas = Order::select([
                DB::raw('count(id) as `client`'), 
                DB::raw('DATE(created_at) as day'),
                DB::raw('sum(total) as total'),
                DB::raw('sum(discount) as discount')
              ])->groupBy('day')
              // And restrict these results to only those created in the last week
              ->whereMonth('created_at', Carbon::parse($sort)->format('m'))
              ->get();

            return view('reports.monthly', compact('datas', 'month'));
        }


    }
}
