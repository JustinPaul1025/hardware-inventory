<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Spatie\Activitylog\Models\Activity;
use App\Http\Resources\Log\LogCollection;

class LogController extends Controller
{
    public function index()
    {
        return new LogCollection(Activity::all());
    }

    public function clear()
    {
        return Artisan::call("activitylog:clean");
    }
}
