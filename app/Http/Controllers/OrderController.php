<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Requests\OrderRequest;
use App\Http\Resources\Order\OrderCollection;
use App\Http\Resources\Order\Order as OrderResource;

class OrderController extends Controller
{
    public function index()
    {
        return view('orders.index');
    }

    public function list()
    {
        return new OrderCollection(Order::paginate(10));
    }

    public function search(Request $request)
    {
        $data = Order::where('dr_number', 'LIKE', '%'.$request->input('search').'%')
                ->orWhere('deliver_to', 'LIKE', '%'.$request->input('search').'%')
                ->get();
        
        return new OrderCollection($data);
    }

    public function show(Order $order)
    {
        return new OrderResource($order);
    }

    public function update(Order $order, OrderRequest $request)
    {
        return $order->update([
            'dr_number' => $request->input('dr_number'),
            'deliver_to' => $request->input('deliver_to'),
            'discount' => $request->input('discount')
        ]);
    }

    public function store(OrderRequest $request)
    {
        beginTransaction();
        try {
            $order = Order::create([
                'dr_number' => $request->input('dr_number'),
                'deliver_to' => $request->input('deliver_to'),
                'total' => $request->input('total'),
                'discount' => $request->input('discount')
            ]);

            $total = 0;

            activity()
                ->causedBy(auth()->user())
                ->performedOn($order)
                ->log('created order dr no. '.$order->dr_number);

            foreach ($request->input('products') as $product) {
                //dd($product['id']);
                $order->orderItems()->create([
                    'product_id' => $product['id'],
                    'quantity' => $product['pcs']
                ]);

                activity()
                    ->causedBy(auth()->user())
                    ->performedOn($order)
                    ->withProperties(['item' => $product['name'], 'quantity' => $product['pcs']])
                    ->log('add item to dr no. '.$order->dr_number);

                $productUpdate = Product::where('id', $product['id'])->first();
                $total = $total + ($productUpdate->price * $product['pcs']);
                $productUpdate->update([
                    'quantity' => $productUpdate->quantity - $product['pcs']
                ]);
            }

            $order->update([
                'total' => $total
            ]);
                
            commit();
        } catch (Exception $e) {
            rollback();
            throw $e;
        }

        return response()->json($order);
    }

    public function delete(Order $order)
    {
        foreach ($order->orderItems as $product) {
            $product->product()->update([
                'quantity' => $product->product->quantity + $product->quantity
            ]);
        }
        return $order->delete();
    }
}
