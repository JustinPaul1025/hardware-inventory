<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\Product\ProductCollection;
use App\Http\Resources\Product\Product as ProductResource;

class ProductController extends Controller
{
    public function index()
    {
        return view('products.index');
    }

    public function productList()
    {
        return new ProductCollection(Product::paginate(10)); 
    }

    public function search(Request $request)
    {
        $data = Product::where('name', 'LIKE', '%'.$request->input('search').'%')->get();
        return new ProductCollection($data); 
    }

    public function show(Product $product)
    {
        return new ProductResource($product);
    }

    public function store(ProductRequest $request)
    {
        return Product::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'category_id' => $request->input('category'),
            'unit_id' => $request->input('unit'),
            'price' => $request->input('price'),
            'quantity' => $request->input('quantity')
        ]);
    }

    public function update(Product $product, ProductRequest $request)
    {
        $product->update([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'category_id' => $request->input('category'),
            'unit_id' => $request->input('unit'),
            'price' => $request->input('price'),
            'quantity' => $request->input('quantity')
        ]);

        return new ProductResource($product);
    }

    public function delete(Product $product)
    {
        return $product->delete();
    }

    public function inStock(Product $product, Request $request)
    {
        $request->validate([
            'quantity' => 'required',
        ]);

        $product->update([
            'quantity' => $request->input('quantity') + $product->quantity
        ]);

        return response()->json('success');
    }

    public function list()
    {
        return new ProductCollection(Product::get());
    }
}
