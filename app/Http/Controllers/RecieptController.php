<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Http\Request;

class RecieptController extends Controller
{
    public function print(Order $order)
    {
        $order = $order;
        $items = OrderItem::where('order_id', $order->id)->with('product')->get();

        return view('reports.reciept', compact('order', 'items'));
    }
}
