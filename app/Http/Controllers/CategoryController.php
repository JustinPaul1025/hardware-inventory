<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Spatie\Activitylog\Contracts\Activity;
use App\Http\Resources\CategoriesCollection;

class CategoryController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255', 'unique:categories']
        ]);

        $category = Category::create([
            'name' => $request->input('name')
        ]);

        activity()
            ->causedBy(auth()->user())
            ->performedOn($category)
            ->withProperties(['category' => $category->name])
            ->log('creating category');


        return $category;
    }

    public function update(Category $category, Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255', 'unique:categories,name,'.$category->id]
        ]);

        $from = $category->name;

        $category->update([
            'name' => $request->input('name')
        ]);

        activity()
            ->causedBy(auth()->user())
            ->performedOn($category)
            ->withProperties(['from' => $from, 'to' => $category->name])
            ->log('editing category');

        return $category;
    }

    public function getSelection()
    {
        return new CategoriesCollection(Category::orderBy('name', 'asc')->get());
    }
}
