<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use App\Http\Requests\OrderItemRequest;
use App\Http\Resources\OrderItem\OrderItemCollection;

class OrderItemController extends Controller
{
    public function orderItems(Order $order)
    {
        return new OrderItemCollection($order->orderItems);
    }

    public function update(Order $order, OrderItemRequest $request)
    {
        $match = ['order_id' => $order->id, 'product_id' => $request->input('id')];
        $result = OrderItem::where($match)->first();

        if($result) {
            $addProduct = $result->quantity;
            $product = Product::where('id', $result->product_id)->first();
            $product->update([
                'quantity' => ($product->quantity + $addProduct) - $request->input('pcs')
            ]);

            $oldQty = $result->quantity;

            $result->update([
                'quantity' => $request->input('pcs')
            ]);

            if($result->quantity == 0) {
                $result->delete();
            };

            activity()
                ->causedBy(auth()->user())
                ->performedOn($order)
                ->withProperties(['item' => $result->name, 'old_quantity' => $oldQty,'new quantity' => $request->input('pcs')])
                ->log('update item from dr no. '.$order->dr_number);
        } else {
            $order->orderItems()->create([
                'product_id' => $request->input('id'),
                'quantity' => $request->input('pcs')
            ]);

            $product = Product::where('id', $request->input('id'))->first();
            $product->update([
                'quantity' => $product->quantity - $request->input('pcs')
            ]);

            activity()
                ->causedBy(auth()->user())
                ->performedOn($order)
                ->withProperties(['item' => $product->name, 'quantity' => $request->input('pcs')])
                ->log('add item to dr no. '.$order->dr_number);
        }
        
        $total = 0;
        foreach ($order->orderItems as $item) {
            $product = Product::where('id', $item->product_id)->first();
            $total = $total + ($item->quantity * $product->price);
        }

        $order->update([
            'total' => $total
        ]);

        return response()->json('success');
    }

    public function delete(OrderItem $orderItem, Request $request)
    {
        $order = Order::where('id', $orderItem->order_id)->first();
        $product = Product::where('id', $orderItem->product_id)->first();
        $quantity = $orderItem->quantity;

        $product->update([
            'quantity' => (float)$product->quantity + (float)$quantity
        ]);

        $order->update([
            'total' => $order->total - $request->input('price')
        ]);

        activity()
            ->causedBy(auth()->user())
            ->performedOn($order)
            ->withProperties(['deleted item' => $product->name, 'item price' => $request->input('price')])
            ->log('delete item from dr no. '.$order->dr_number);

        return $orderItem->delete();
    }
}
