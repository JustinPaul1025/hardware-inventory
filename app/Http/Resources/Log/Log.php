<?php

namespace App\Http\Resources\Log;

use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Log extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'description' => $this->description,
            'user' => User::where('id', $this->causer_id)->first(),
            'properties' => $this->properties,
            'created_at' => Carbon::parse($this->created_at)->format('M d Y h:i:s')
        ];
    }
}
