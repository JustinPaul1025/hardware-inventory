<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'category_name' => $this->category_id != null ? $this->category->name : '',
            'price' => $this->price,
            'updated_at' => $this->updated_at,
            'quantity' => $this->quantity,
            'unit_name' => $this->unit_id != null ? $this->unit->name : '',
            'unit' => $this->unit_id,
            'category' => $this->category_id
        ];
    }
}
