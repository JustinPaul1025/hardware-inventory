<?php

namespace App\Http\Resources\Order;

use Illuminate\Http\Resources\Json\JsonResource;

class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'dr_number' => $this->dr_number,
            'deliver_to' => $this->deliver_to,
            'discount' => $this->discount,
            'total' => $this->total
        ];
    }
}
