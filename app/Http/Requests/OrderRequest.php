<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dr_number' => ['required', 'string', 'max:255'],
            'deliver_to' => ['required', 'string', 'max:255'],
            'discount' => 'required',
            'total' => 'required'
        ];
    }
}
