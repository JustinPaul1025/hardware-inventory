require('./bootstrap');

import Swal from 'sweetalert2'
import VuejsPaginate from 'vuejs-paginate'

window.Vue = require('vue');
window.Swal = Swal;

Vue.mixin({
    computed: {
        app: () => window.App
    }
})

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('product', require('./components/Product/Index.vue').default);
Vue.component('order', require('./components/Order/Index.vue').default);
Vue.component('user', require('./components/User/Index.vue').default);
Vue.component('utilities', require('./components/Utilities/Index.vue').default);
Vue.component('reports', require('./components/Report/Index.vue').default);
Vue.component('logs', require('./components/Log/Index.vue').default);
Vue.component('paginate', VuejsPaginate);


const app = new Vue({
    el: '#app',
});
