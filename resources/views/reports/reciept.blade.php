<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    
    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <style>
        table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
          width: 1000px;
          margin-left: auto;
          margin-right: auto;
        }
        
        td, th {
          border: 1px solid #dddddd;
          text-align: left;
          padding: 2px;
        }
        
        tr:nth-child(even) {
          background-color: #dddddd;
        }
    </style>
</head>
<body>
    <div class="text-center mb-5 mx-auto" style="width: 300px">
        <p class="text-2xl font-bold">Lion Builder Supply</p>
        <p class="text-sm">
           Tadeco Road, Brgy. Quezon Panabo City
        </p>
    </div>
    <div class="mb-5 flex">
        <div class="w-1/2">
          <b>To:</b>
          {{ $order->deliver_to }}
        </div>
        <div class="w-1/2">
          <b>Delivery No.</b>
          {{ $order->dr_number }}
        </div>
    </div>
    <div>
      <table>
        <tr>
          <th>Item</th>
          <th>Price</th>
          <th>Qty.</th>
          <th></th>
        </tr>
        <?php $totalPaid = 0 ?>
        @foreach ($items as $item)
            <tr>
              <td>{{ $item->product->name }}</td>
              <td>₱ {{ $item->product->price }}</td>
              <td>{{ $item->quantity }}</td>
              <td>₱ {{ $item->quantity * $item->product->price }}</td>
              <?php $totalPaid = ($item->quantity * $item->product->price) + $totalPaid ?>
            </tr>
        @endforeach
        <tr style="background-color: black">
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
            <td><b>Discount:</b></td>
            <td>₱ {{ $order->discount }}</td>
            <td><b>Total:</b></td>
            <td>₱ {{ $totalPaid }}</td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td><b>Total Paid:</b></td>
          <td>₱ {{ $totalPaid - $order->discount }}</td>
      </tr>
      </table>
    </div>
    <div>
        
    </div>
</body>
</html>