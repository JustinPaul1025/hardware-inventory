<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    
    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <style>
        table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
          width: 1000px;
          margin-left: auto;
          margin-right: auto;
        }
        
        td, th {
          border: 1px solid #dddddd;
          text-align: left;
          padding: 2px;
        }
        
        tr:nth-child(even) {
          background-color: #dddddd;
        }
    </style>
</head>
<body>
    <div class="text-center mb-5 mx-auto" style="width: 300px">
        <p class="text-2xl font-bold">Lion Builder Supply</p>
        <p class="text-sm">
           Tadeco Road, Brgy. Quezon Panabo City
        </p>
    </div>
    <div class="text-center mb-5">
        <p class="font-bold">Income Report as of {{$date}}</p>
    </div>
    <div>
        <table>
            <tr>
              <th>Dr no.</th>
              <th>Delivered to</th>
              <th>Total</th>
              <th>Discount</th>
              <th>Paid</th>
            </tr>
            <?php $totalPaid = 0 ?>
            @foreach ($datas as $data)
                <tr>
                    <td>{{ $data->dr_number }}</td>
                    <td>{{ $data->deliver_to }}</td>
                    <td>₱ {{ $data->total }}</td>
                    <td>₱ {{ $data->discount }}</td>
                    <td>₱ {{ ($data->total - $data->discount) }}</td>
                </tr>
            <?php $totalPaid = ($data->total - $data->discount) + $totalPaid ?>
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td><b>Total Paid:</b></td>
                <td>₱ {{ $totalPaid }}</td>
            </tr>
        </table>
    </div>
</body>
</html>