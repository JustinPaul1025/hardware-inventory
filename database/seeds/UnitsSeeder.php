<?php

use App\Models\Unit;
use Illuminate\Database\Seeder;

class UnitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Unit::create([
            'name' => 'meter',
        ]);

        Unit::create([
            'name' => 'pc',
        ]);

        Unit::create([
            'name' => 'feet',
        ]);
    }
}
